<?php

use yii\db\Migration;

class m200517_052237_create_table_user extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string(500),
            'password' => $this->string(100),
            'status' => $this->tinyInteger(4),
            'password_reset_token' => $this->string(500),
            'passwordResetTokenExpire' => $this->string(100),
            'verification_token' => $this->string(500),
            'rememberMe' => $this->integer(),
            'add_date' => $this->dateTime(),
            'update_date' => $this->dateTime(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
