<?php

if (file_exists(__DIR__ . "/../../env.php")) {
    require(__DIR__ . "/../../env.php");
}

$env = isset($ENVIRONMENT) ? $ENVIRONMENT : "local";
define('ENVIRONMENT', $env);
require(__DIR__ . '/../../vendor/autoload.php');
require(__DIR__ . '/../../vendor/yiisoft/yii2/Yii.php');

$config = yii\helpers\ArrayHelper::merge(
    require(__DIR__ . '/../../common/config/main.php'),
    require(__DIR__ . "/../../common/config/main-$env.php"),
    require(__DIR__ . '/../../common/config/bootstrap.php'),
    require(__DIR__ . '/../config/main.php'),
    require(__DIR__ . "/../config/main-$env.php"),
    require(__DIR__ . '/../config/bootstrap.php')
);

$application = new yii\web\Application($config);
//ob_start();
$application->run();