<?php

namespace backend\controllers;

use Yii;
use common\models\News;
use common\models\Product;
use common\models\Model;
use common\models\Author;
use common\models\search\NewsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\UploadedFile;
use  yii\helpers\ArrayHelper;
/**
 * NewsController implements the CRUD actions for News model.
 */
class NewsController extends Controller
{
     /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update'],
                        'roles' => ['manageNews'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    throw new \Exception('You are not allowed to access this page');
                }
            ],
        ];
    }

    /**
     * Lists all News models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new NewsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single News model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new News model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new News();
        $product = Product::find()->where(['status' => 1])->asArray()->all();
        $productList = ArrayHelper::map($product, 'id_product', 'title');
        $author = Author::find()->asArray()->all();
        $authorList = ArrayHelper::map($author, 'id_author', 'name');
        if ($model->load(Yii::$app->request->post())) {
            $coverImage = UploadedFile::getInstances($model, 'coverImage');
            if(!empty($coverImage[0])) {
                $fileName = Yii::$app->image->upload($coverImage[0], 'news-image/');
                $model->image = $fileName;
            }
            if($model->validate()){
                if($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id_news]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'productList' => $productList,
            'authorList' => $authorList
        ]);
    }

    /**
     * Updates an existing News model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $product = Product::find()->where(['status' => 1])->asArray()->all();
        $productList = ArrayHelper::map($product, 'id_product', 'title');
        $author = Author::find()->asArray()->all();
        $authorList = ArrayHelper::map($author, 'id_author', 'name');
        $modelData = Model::find()->where(['status' => 1, 'id_model' => $model->id_model])->asArray()->all();
        $modelList = ArrayHelper::map($modelData, 'id_model', 'model_name');
        if ($model->load(Yii::$app->request->post())) {
            $coverImage = UploadedFile::getInstances($model, 'coverImage');
            if(!empty($coverImage[0])) {
                $fileName = Yii::$app->image->upload($coverImage[0], 'news-image/');
                $model->image = $fileName;
            }
            if($model->validate()){
                if($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id_news]);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'productList' => $productList,
            'authorList' => $authorList,
            'modelList' => $modelList
        ]);
    }

    /**
     * Deletes an existing News model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the News model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return News the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = News::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
