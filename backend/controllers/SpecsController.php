<?php

namespace backend\controllers;

use Yii;
use common\models\Specs;
use common\models\search\SpecsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\UploadedFile;
use  yii\helpers\ArrayHelper;;
use yii\widgets\ActiveForm;
// use app\base\Model;
/**
 * SpecsController implements the CRUD actions for Specs model.
 */
class SpecsController extends Controller
{
     /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update'],
                        'roles' => ['manageSpecs'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    throw new \Exception('You are not allowed to access this page');
                }
            ],
        ];
    }

    /**
     * Lists all Specs models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SpecsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Specs model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Specs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Specs();
        $params = Yii::$app->request->get();
        if ($model->load(Yii::$app->request->post())) {

            $modelClass = Specs::createMultiple(Specs::classname());
            Specs::loadMultiple($modelClass, Yii::$app->request->post());

            $valid = $model->validate();
            $valid = Specs::validateMultiple($modelClass) && $valid;
            
            if ($valid) {
                $model->id_product = $params['id_product'];
                $model->id_model = $params['id_model'];
                $specsData = [];
                foreach ($modelClass as $key => $value) {
                    if(!empty($value->value) && !empty($value->name)) {
                        $specsData[] = [
                            'name' => $value->name,
                            'value' => $value->value,
                        ];
                    }
                }
                $model->specs = json_encode($specsData);
                $model->save();
                return $this->redirect(['update', 'id' => $model->id_specs, 'id_product' => $model->id_product, 'id_model' => $model->id_model]);
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelClass' => [$model],
        ]);
    }

    /**
     * Updates an existing Specs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $params = Yii::$app->request->get();
        $specs = json_decode($model->specs, true);
        $modelClass = [];
        foreach ($specs as $key => $value) {
            $modelClass[$key] = ArrayHelper::merge(clone $model, $value);
        }
        if ($model->load(Yii::$app->request->post())) {

            $modelClass = Specs::createMultiple(Specs::classname());
            Specs::loadMultiple($modelClass, Yii::$app->request->post());

            $valid = $model->validate();
            $valid = Specs::validateMultiple($modelClass) && $valid;
            
            if ($valid) {
                $model->id_product = $params['id_product'];
                $model->id_model = $params['id_model'];
                $specsData = [];
                foreach ($modelClass as $key => $value) {
                    if(!empty($value->value) && !empty($value->name)) {
                        $specsData[] = [
                            'name' => $value->name,
                            'value' => $value->value,
                        ];
                    }
                }
                $model->specs = json_encode($specsData);
                $model->save();
                return $this->redirect(['update', 'id' => $model->id_specs, 'id_product' => $model->id_product, 'id_model' => $model->id_model]);
            }
        }

        return $this->render('update', [
            'model' => $model,
            'modelClass' => $modelClass,
        ]);
    }

    /**
     * Deletes an existing Specs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Specs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Specs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Specs::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
