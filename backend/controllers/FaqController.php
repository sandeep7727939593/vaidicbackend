<?php

namespace backend\controllers;

use Yii;
use common\models\Faq;
use common\models\search\FaqSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use  yii\helpers\ArrayHelper;
use common\models\Product;
use common\models\Model;
/**
 * FaqController implements the CRUD actions for Faq model.
 */
class FaqController extends Controller
{
     /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update'],
                        'roles' => ['manageFaq'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    throw new \Exception('You are not allowed to access this page');
                }
            ],
        ];
    }

    /**
     * Lists all Faq models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FaqSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Faq model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Faq model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Faq();
        $product = Product::find()->where(['status' => 1])->asArray()->all();
        $productList = ArrayHelper::map($product, 'id_product', 'title');
        if ($model->load(Yii::$app->request->post())) {
            if($model->validate()) {
                $model->save();
                return $this->redirect(Url::toRoute('faq/index', true));
            }
        }

        return $this->render('create', [
            'model' => $model,
            'productList' => $productList,
        ]);
    }

    /**
     * Updates an existing Faq model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $product = Product::find()->where(['status' => 1])->asArray()->all();
        $productList = ArrayHelper::map($product, 'id_product', 'title');
        $modelData = Model::find()->where(['status' => 1, 'id_model' => $model->id_model])->asArray()->all();
        $modelList = ArrayHelper::map($modelData, 'id_model', 'model_name');
        if ($model->load(Yii::$app->request->post())) {
            if($model->validate()) {
                $model->save();
                return $this->redirect(Url::toRoute('faq/index', true));
            }
        }

        return $this->render('update', [
            'model' => $model,
            'productList' => $productList,
            'modelList' => $modelList,
        ]);
    }

    /**
     * Deletes an existing Faq model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Faq model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Faq the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Faq::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
