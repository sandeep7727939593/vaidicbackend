<?php

namespace backend\controllers;

use Yii;
use common\models\Model;
use common\models\Product;
use common\models\search\ModelSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\UploadedFile;
use  yii\helpers\ArrayHelper;

/**
 * ModelController implements the CRUD actions for Model model.
 */
class ModelController extends Controller
{
     /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update'],
                        'roles' => ['manageModel'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    throw new \Exception('You are not allowed to access this page');
                }
            ],
        ];
    }
    /**
     * Lists all Model models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ModelSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Model model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $product = Product::find(['id_product' => $model->id_product])->asArray()->one();
        $model->id_product = $product['title'];
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Model model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Model();
        $product = Product::find()->asArray()->all();
        $productList = ArrayHelper::map($product, 'id_product', 'title');
        if ($model->load(Yii::$app->request->post())) {
            if($model->validate()){
                $image = UploadedFile::getInstances($model, 'file');
                if(!empty($image[0])) {
                    $fileName = Yii::$app->image->upload($image[0], 'model/');
                    $model->cover_image = $fileName;
                }
                if($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id_model]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'productList' => $productList,
        ]);
    }

    /**
     * Updates an existing Model model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $product = Product::find()->asArray()->all();
        $productList = ArrayHelper::map($product, 'id_product', 'title');
        if ($model->load(Yii::$app->request->post())) {
            if($model->validate()){
                $image = UploadedFile::getInstances($model, 'file');
                if(!empty($image[0])) {
                    $fileName = Yii::$app->image->upload($image[0], 'model/');
                    $model->cover_image = $fileName;
                }
                if($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id_model]);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'productList' => $productList,
        ]);
    }

    /**
     * Deletes an existing Model model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Model model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Model the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Model::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
