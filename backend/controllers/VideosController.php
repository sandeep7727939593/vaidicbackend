<?php

namespace backend\controllers;

use Yii;
use common\models\Videos;
use common\models\Product;
use common\models\Model;
use common\models\Author;
use common\models\search\VideosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\UploadedFile;
use  yii\helpers\ArrayHelper;
/**
 * VideosController implements the CRUD actions for Videos model.
 */
class VideosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update'],
                        'roles' => ['manageVideos'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    throw new \Exception('You are not allowed to access this page');
                }
            ],
        ];
    }

    /**
     * Lists all Videos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VideosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Videos model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Videos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Videos();
        $product = Product::find()->where(['status' => 1])->asArray()->all();
        $productList = ArrayHelper::map($product, 'id_product', 'title');
        $author = Author::find()->asArray()->all();
        $authorList = ArrayHelper::map($author, 'id_author', 'name');

        if ($model->load(Yii::$app->request->post())) {
            $coverImage = UploadedFile::getInstances($model, 'coverImage');
            if(!empty($coverImage[0])) {
                $fileName = Yii::$app->image->upload($coverImage[0], 'videos-image/');
                $model->image = $fileName;
            }
            $videoUpload = UploadedFile::getInstances($model, 'videoUpload');
            if(!empty($videoUpload[0])) {
                $fileName = Yii::$app->image->videoUpload($videoUpload[0], 'videos/');
                $model->video_value = $fileName;
            }
            if($model->validate()){
                if($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id_videos]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
            'productList' => $productList,
            'authorList' => $authorList
        ]);
    }

    /**
     * Updates an existing Videos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $product = Product::find()->where(['status' => 1])->asArray()->all();
        $productList = ArrayHelper::map($product, 'id_product', 'title');
        $modelData = Model::find()->where(['status' => 1, 'id_model' => $model->id_model])->asArray()->all();
        $modelList = ArrayHelper::map($modelData, 'id_model', 'model_name');
        $author = Author::find()->asArray()->all();
        $authorList = ArrayHelper::map($author, 'id_author', 'name');
        if ($model->load(Yii::$app->request->post())) {
            $coverImage = UploadedFile::getInstances($model, 'coverImage');
            if(!empty($coverImage[0])) {
                $fileName = Yii::$app->image->upload($coverImage[0], 'videos-image/');
                $model->image = $fileName;
            }
            $videoUpload = UploadedFile::getInstances($model, 'videoUpload');
            if(!empty($videoUpload[0])) {
                $fileName = Yii::$app->image->videoUpload($videoUpload[0], 'videos/');
                $model->video_value = $fileName;
            }
            if($model->validate()){
                if($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id_videos]);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
            'productList' => $productList,
            'authorList' => $authorList,
            'modelList' => $modelList,
        ]);
    }

    /**
     * Deletes an existing Videos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Videos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Videos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Videos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
