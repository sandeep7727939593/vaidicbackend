<?php

namespace backend\controllers;

use Yii;
use common\models\Benefit;
use common\models\search\BenefitSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Url;
use  yii\helpers\ArrayHelper;
use common\models\Product;
use common\models\Model;
/**
 * BenefitController implements the CRUD actions for Benefit model.
 */
class BenefitController extends Controller
{
   /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'create', 'update'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'create', 'update'],
                        'roles' => ['manageBenefit'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    throw new \Exception('You are not allowed to access this page');
                }
            ],
        ];
    }


    /**
     * Lists all Benefit models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BenefitSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Benefit model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Benefit model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Benefit();
        $product = Product::find()->where(['status' => 1])->asArray()->all();
        $productList = ArrayHelper::map($product, 'id_product', 'title');
        if ($model->load(Yii::$app->request->post())) {
            $modelClass = Benefit::createMultiple(Benefit::classname());
            Benefit::loadMultiple($modelClass, Yii::$app->request->post());

            $valid = $model->validate();
            $valid = Benefit::validateMultiple($modelClass) && $valid;
            
            if ($valid) {
                $benefitData = [];
                foreach ($modelClass as $key => $value) {
                    if(!empty($value->title) && !empty($value->description)) {
                        $benefitData[] = [
                            'title' => $value->title,
                            'description' => $value->description,
                        ];
                    }
                }
                $model->value = json_encode($benefitData);
                $model->save();
                return $this->redirect(Url::toRoute('benefit/index', true));
            }
        }

        return $this->render('create', [
            'model' => $model,
            'modelClass' => [$model],
            'productList' => $productList,
        ]);
    }

    /**
     * Updates an existing Benefit model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $benefit = json_decode($model->value, true);
        $modelClass = [];
        foreach ($benefit as $key => $v) {
            $modelClass[$key] = ArrayHelper::merge(clone $model, $v);
        }

        $product = Product::find()->where(['status' => 1])->asArray()->all();
        $productList = ArrayHelper::map($product, 'id_product', 'title');
        $modelData = Model::find()->where(['status' => 1, 'id_model' => $model->id_model])->asArray()->all();
        $modelList = ArrayHelper::map($modelData, 'id_model', 'model_name');
        if ($model->load(Yii::$app->request->post())) {
            $modelClass = Benefit::createMultiple(Benefit::classname());
            Benefit::loadMultiple($modelClass, Yii::$app->request->post());

            $valid = $model->validate();
            $valid = Benefit::validateMultiple($modelClass) && $valid;
            
            if ($valid) {
                $benefitData = [];
                foreach ($modelClass as $key => $value) {
                    if(!empty($value->title) && !empty($value->description)) {
                        $benefitData[] = [
                            'title' => $value->title,
                            'description' => $value->description,
                        ];
                    }
                }
                $model->value = json_encode($benefitData);
                $model->save();
                return $this->redirect(Url::toRoute('benefit/index', true));
            }
        }
        return $this->render('update', [
            'model' => $model,
            'modelClass' => $modelClass,
            'productList' => $productList,
            'modelList' => $modelList
        ]);
    }

    /**
     * Deletes an existing Benefit model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Benefit model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Benefit the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Benefit::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
