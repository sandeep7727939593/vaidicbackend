<?php

namespace backend\controllers;

use Yii;
use common\models\Videos;
use common\models\Model;
use common\models\Author;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use  yii\helpers\ArrayHelper;
/**
 * VideosController implements the CRUD actions for Videos model.
 */
class CommonController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['model-by-brand', 'upload-editor-img'],
                'rules' => [                    
                    [
                        'allow' => true,
                        'actions' => ['model-by-brand', 'upload-editor-img'],
                        'roles' => ['manageCommon'],
                    ],
                ],
                'denyCallback' => function ($rule, $action) {
                    return $this->redirect(['site/login'])->send();
                }
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeAction($action)
    {            
        if ($action->id == 'upload-editor-img') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionModelByBrand()
    {   
        if(isset($_POST['depdrop_parents']))
        {
            $parents = $_POST['depdrop_parents'];
            if($parents != null)
            {
                $brandId = $parents[0];
                
                $modelServiceObj = Yii::$container->get("services\ModelServiceInterface");

                $models = $modelServiceObj->getList(['id_brand' => $brandId], ["model_name as name","id_model as id"], 1, "id_model asc", -1, null, [], ["as_array" => true]);

                return json_encode(['output'=>$models,'selected'=>'']);
            }
        }
        return json_encode(['output'=>'','selected'=>'']);
    }


    public function actionGetModelByProduct() {
        $id_product = Yii::$app->request->get('id_product');
        if(!empty($id_product)) {
            $model = Model::find()->where(['status' => 1, 'id_product' => $id_product])->asArray()->all();
            $text = '<option>Select Model</option>';
            foreach($model as $value) {
                $text .= '<option value=' . $value['id_model'] . '>' . $value['model_name'] . '</option>';
            }
            echo $text;
        }
    }

    public function actionUploadEditorMedia()
    {
        $processedImagePath = 'upload/'. Yii::$app->params['editorMedia'];
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if(Yii::$app->request->post()) {
                reset ($_FILES);
                $temp = current($_FILES);
                $ext  = pathinfo($temp['name'], PATHINFO_EXTENSION);
                
                if (!in_array(strtolower($ext),['mkv', 'mp4'])) {
                    echo json_encode(['message' => 'Invalid File Type.']);
                }
                else
                {
                    if($temp['size'] > 0 && $temp['size'] <= 2097152) {
                        $newfileName = uniqid() .'.'. $ext; 
                        $filetowrite = $processedImagePath . $newfileName;
                        
                        //copy($temp['tmp_name'], $filetowrite);
                        $result = Yii::$app->s3->commands()->upload($filetowrite, $temp["tmp_name"])->withAcl('public-read')->execute();
                        $url = Yii::$app->s3->getUrl('upload/editorMedia/' . $newfileName);
                        
                        echo json_encode(['path' => $url]);
                    } else {
                        echo json_encode(['message' => 'Invalid File Size.']);
                    }                    
                }
            }
            else {
                header("HTTP/1.1 500 Internal Server Error or Invalid Request.");
                return;
            }
        }
    }

}
