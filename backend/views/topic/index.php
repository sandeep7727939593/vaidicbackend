<?php

use yii\helpers\Html;
// use yii\grid\GridView;
use kartik\grid\GridView;
use common\models\Category;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\Topic */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Topics');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="topic-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Topic'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_topic',
            [
                'attribute'=>'category',
                'filter'=>ArrayHelper::map(Category::find()->asArray()->all(), 'category', 'category'),
                'value' => function($model, $attribute, $key, $index) {
                    $value = Category::find()->where(['id_category' => $model->id_category])->asArray()->one();
                    return $value['category'];
                }
            ],
            'title',
            // 'link_rewrite',
            'side_menu_title',
            // 'description:ntext',
            'status',
            //'index_position',
            //'created_by_id',
            //'updated_by_id',
            //'add_date',
            'update_date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
