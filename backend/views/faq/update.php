<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Faq */

$this->title = Yii::t('app', 'Update Faq: {name}', [
    'name' => $model->id_faq,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Faqs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_faq, 'url' => ['view', 'id' => $model->id_faq]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="faq-update">

    <?= $this->render('_form', [
        'model' => $model,
        'productList' => $productList,
        'modelList' => $modelList,
    ]) ?>

</div>
