<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Faq */
/* @var $form yii\widgets\ActiveForm */
$clientOptions = Yii::$app->params['clientOptions'];

?>

<div class="faq-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_product')->dropDownList($productList, 
        [
            'prompt'=>'Choose Product',
            'onchange' => '$.get( "'.Yii::$app->urlManager->createUrl('common/get-model-by-product?id_product=').'"+$(this).val(), function( data ) {
                console.log("data", data)
                  $("#faq-id_model" ).html( data );
                });'
        ]) ?>

    <?= $form->field($model, 'id_model')->dropDownList($modelList, 
        [
            'prompt'=>'Choose Model',
        ]) ?>

    <?= $form->field($model, 'question')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'answer')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
