<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Product;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\ModelSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Models');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model-index">

    <p>
        <?= Html::a(Yii::t('app', 'Create Model'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id_model',
            // 'id_product',
            [
                'attribute' => 'id_product',
                'label' => 'Product Name',
                'filter' => Product::dropdown(),
                 'value' => function($model, $index, $dataColumn) {
                        return $model->product->title;
                    },            
            ],
            // 'cover_image',
            'model_name',
            // 'short_name',
            'link_rewrite',
            //'base_price',
            //'description:ntext',
            //'popularity',
            //'alternative_link_rewrite',
            //'alternative_link_type',
            'status',
            //'add_date',
            //'launch_date',
            //'update_date',
            //'created_by_id',
            //'updated_by_id',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}{update}{specs}',
                'buttons' => [
                    'specs' => function($url, $model, $key) {
                        $url = !empty($model->specs) ? ['specs/update?id_product='.$model->id_product . '&id_model=' . $model->id_model . '&id=' . $model->specs->id_specs] : ['specs/create?id_product='.$model->id_product . '&id_model=' . $model->id_model];
                        $className = !empty($model->specs) ? 'btn btn-warning btn-xs' : 'btn btn-success btn-xs';
                        $buttonName = !empty($model->specs) ? 'Specs Update' : 'Specs Create';
                        return Html::a($buttonName, $url, ['class' => $className]);
                    }
                ]
            ],
        ],
    ]); ?>


</div>
