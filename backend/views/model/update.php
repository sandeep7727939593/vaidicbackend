<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Model */

$this->title = Yii::t('app', 'Update Model: {name}', [
    'name' => $model->id_model,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Models'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_model, 'url' => ['view', 'id' => $model->id_model]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="model-update">

    <?= $this->render('_form', [
        'model' => $model,
        'productList' => $productList,
    ]) ?>

</div>
