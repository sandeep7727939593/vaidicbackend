<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Model */

$this->title = $model->id_model;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Models'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="model-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id_model], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_model',
            'id_product',
            'cover_image',
            'model_name',
            'short_name',
            'link_rewrite',
            'base_price',
            'description:ntext',
            'popularity',
            'alternative_link_rewrite',
            'alternative_link_type',
            'status',
            'add_date',
            'launch_date',
            'update_date',
            'created_by_id',
            'updated_by_id',
        ],
    ]) ?>

</div>
