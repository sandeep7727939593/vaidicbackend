<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Model */

$this->title = Yii::t('app', 'Create Model');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Models'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model-create">

    <?= $this->render('_form', [
        'model' => $model,
        'productList' => $productList,
    ]) ?>

</div>
