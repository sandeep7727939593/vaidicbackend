<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
use yii\jui\DatePicker;


$imgUrl = Yii::$app->params['mediaUrl'] . 'model/';
/* @var $this yii\web\View */
/* @var $model common\models\Model */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="model-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_product')->dropDownList($productList, ['prompt'=>'Choose Product']) ?>

    <?= $form->field($model, 'model_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'file')->fileInput() ?>
    <?php if(!$model->isNewRecord) { ?> 
        <div>
            <img src="<?= $imgUrl. $model->cover_image; ?>" width="100" height="100" />
        </div>
    <?php } ?>

    <?= $form->field($model, 'base_price')->textInput() ?>

     <?= $form->field($model, 'description')->widget(TinyMce::className(), [
        'options' => ['rows' => 10],
        'language' => 'en',
        'clientOptions' => Yii::$app->params['clientOptions']
    ]);?>

    <?= $form->field($model, 'popularity')->textInput() ?>
    <?php if(!$model->isNewRecord) { ?>
        <?= $form->field($model, 'alternative_link_rewrite')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'alternative_link_type')->dropDownList([], ['prompt'=>'Choose Redirection type']) ?>
    <?php } ?>


    <?= $form->field($model, 'launch_date')->widget(
      DatePicker::className(), [
        'language' => 'en',
        'dateFormat' => 'yyyy-MM-dd',
         'options' => ['placeholder' => 'Select Launch date ...'],
        ]);?>
    <?= $form->field($model, 'status')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
