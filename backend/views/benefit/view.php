<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Benefit */

$this->title = $model->id_benefit;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Benefits'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="benefit-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id_benefit], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_benefit',
            'id_product',
            'id_model',
            'value:ntext',
            'status',
            'add_date',
            'launch_date',
            'update_date',
            'created_by_id',
            'updated_by_id',
        ],
    ]) ?>

</div>
