<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Benefit */

$this->title = Yii::t('app', 'Create Benefit');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Benefits'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="benefit-create">

    <?= $this->render('_form', [
        'model' => $model,
        'modelClass' => $modelClass,
        'productList' => $productList,
        'modelList' => []
    ]) ?>

</div>
