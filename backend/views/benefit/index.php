<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Product;
use common\models\Model;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\BenefitSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Benefits');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="benefit-index">

    <p>
        <?= Html::a(Yii::t('app', 'Create Benefit'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}{update}'],

            'id_benefit',
            [
                'attribute' => 'id_product',
                'label' => 'Product Name',
                'filter' => Product::dropdown(),
                 'value' => function($model, $index, $dataColumn) {
                        if(!empty($model->product)) {
                            return $model->product->title;
                        }
                    },            
            ],
            [
                'attribute' => 'id_model',
                'label' => 'Model Name',
                'filter' => Model::dropdown(),
                 'value' => function($model, $index, $dataColumn) {
                        if(!empty($model->model)) {
                            return $model->model->model_name;
                        }
                    },            
            ],
            'value',
            // 'question',
            //'answer:ntext',
            'status',
            //'add_date',
            //'launch_date',
            //'update_date',
            //'created_by_id',
            //'updated_by_id',

        ],
    ]); ?>


</div>
