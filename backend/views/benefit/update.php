<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Benefit */

$this->title = Yii::t('app', 'Update Benefit: {name}', [
    'name' => $model->id_benefit,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Benefits'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_benefit, 'url' => ['view', 'id' => $model->id_benefit]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="benefit-update">

    <?= $this->render('_form', [
        'model' => $model,
        'modelClass' => $modelClass,
        'productList' => $productList,
        'modelList' => $modelList
    ]) ?>

</div>
