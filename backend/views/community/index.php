<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\CommunitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Communities');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="community-index">

    <p>
        <?= Html::a(Yii::t('app', 'Create Community'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}{update}'],
            'id_post',
            'type',
            'description:ntext',
            'status',
            'add_date',
            //'launch_date',
            //'update_date',
            //'created_by_id',
            //'updated_by_id',

        ],
    ]); ?>


</div>
