<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Community */

$this->title = $model->id_post;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Communities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="community-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id_post], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_post',
            'type',
            'description:ntext',
            'status',
            'add_date',
            'launch_date',
            'update_date',
        ],
    ]) ?>

</div>
