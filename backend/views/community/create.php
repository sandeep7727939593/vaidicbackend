<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Community */

$this->title = Yii::t('app', 'Create Community');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Communities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="community-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
