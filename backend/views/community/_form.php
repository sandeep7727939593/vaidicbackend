<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Community */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="community-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type')->dropDownList(['facebook' => 'Facebook', 'twitter' => 'Twitter'], 
        [
            'prompt'=>'Choose Type',
        ]) ?>
    <div>
        <ul>
            <li>Facebook :- Write Embedded Url </li>
            <li>Tiwtter :- Write Embedded Code </li>
        </ul>
    </div>
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'status')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
