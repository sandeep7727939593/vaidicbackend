<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Community */

$this->title = Yii::t('app', 'Update Community: {name}', [
    'name' => $model->id_post,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Communities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_post, 'url' => ['view', 'id' => $model->id_post]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="community-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
