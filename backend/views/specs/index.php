<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\SpecsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Specs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="specs-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Specs'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_specs',
            'id_model',
            'id_product',
            'specs:ntext',
            'status',
            //'add_date',
            //'launch_date',
            //'update_date',
            //'created_by_id',
            //'updated_by_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
