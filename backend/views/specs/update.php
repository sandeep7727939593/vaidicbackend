<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Specs */

$this->title = Yii::t('app', 'Update Specs: {name}', [
    'name' => $model->id_specs,
]);
?>
<div class="specs-update">

    <?= $this->render('_form', [
        'model' => $model,
        'modelClass' => $modelClass
    ]) ?>

</div>
