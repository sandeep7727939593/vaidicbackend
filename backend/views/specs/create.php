<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Specs */

$this->title = Yii::t('app', 'Create Specs');
?>
<div class="specs-create">

    <?= $this->render('_form', [
        'model' => $model,
        'modelClass' => $modelClass
    ]) ?>

</div>
