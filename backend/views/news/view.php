<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\News */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'News'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="news-view">


    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id_news], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_news',
            'id_product',
            'id_model',
            'content:ntext',
            'id_author:ntext',
            'image:ntext',
            'title',
            'link_rewrite',
            'add_date',
            'update_date',
            'created_by_id',
            'updated_by_id',
            'view_count',
            'status',
        ],
    ]) ?>

</div>
