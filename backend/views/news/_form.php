<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\News */
/* @var $form yii\widgets\ActiveForm */
$imgUrl = Yii::$app->params['mediaUrl'] . 'news-image/';
$clientOptions = Yii::$app->params['clientOptions'];
$clientOptions["images_upload_url"] = Url::toRoute("common/upload-editor-img", true);
$clientOptions['images_upload_base_path'] = Yii::$app->params['mediaUrl'] . 'process-image/';
?>

<div class="news-form">

    <?php $form = ActiveForm::begin(); ?>

     <?= $form->field($model, 'id_product')->dropDownList($productList, 
        [
            'prompt'=>'Choose Product',
            'onchange' => '$.get( "'.Yii::$app->urlManager->createUrl('common/get-model-by-product?id_product=').'"+$(this).val(), function( data ) {
                console.log("data", data)
                  $("#news-id_model" ).html( data );
                });'
        ]) ?>

    <?= $form->field($model, 'id_model')->dropDownList($modelList, 
        [
            'prompt'=>'Choose Model',
        ]) ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->widget(TinyMce::className(), [
        'options' => ['rows' => 20],
        'language' => 'en',
        'clientOptions' => $clientOptions
    ]);?>

    <?= $form->field($model, 'id_author')->dropDownList($authorList, 
        [
            'prompt'=>'Choose Author'
        ]) ?>

    <?= $form->field($model, 'coverImage')->fileInput() ?>
    <?php if(!$model->isNewRecord) { ?> 
        <div>
            <img src="<?= $imgUrl. $model->image; ?>" width="100" height="100" />
        </div>
    <?php } ?>


    <?= $form->field($model, 'status')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
