<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Videos */
/* @var $form yii\widgets\ActiveForm */
$imgUrl = Yii::$app->params['mediaUrl'] . 'videos-image/';
$videoUrl = Yii::$app->params['baseUrl'] . 'videos/';
?>

<div class="videos-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_product')->dropDownList($productList, 
        [
            'prompt'=>'Choose Product',
            'onchange' => '$.get( "'.Yii::$app->urlManager->createUrl('common/get-model-by-product?id_product=').'"+$(this).val(), function( data ) {
                console.log("data", data)
                  $("#videos-id_model" ).html( data );
                });'
        ]) ?>

    <?= $form->field($model, 'id_model')->dropDownList($modelList, 
        [
            'prompt'=>'Choose Model',
        ]) ?>
    <?= $form->field($model, 'video_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'highlight')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'videoUpload')->fileInput() ?>
    <?php if(!$model->isNewRecord) { ?> 
        <div>
            <video width="320" height="240" controls>
                <source src="<?= $videoUrl. $model->video_value; ?>" type="video/mp4">
                Your browser does not support the video tag.
            </video>
        </div>
    <?php } ?>

    <?= $form->field($model, 'coverImage')->fileInput() ?>
    <?php if(!$model->isNewRecord) { ?> 
        <div>
            <img src="<?= $imgUrl. $model->image; ?>" width="100" height="100" />
        </div>
    <?php } ?>

    <?= $form->field($model, 'id_author')->dropDownList($authorList, 
        [
            'prompt'=>'Choose Author'
        ]) ?>

    <?= $form->field($model, 'status')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
