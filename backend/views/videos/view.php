<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Videos */

$this->title = $model->id_videos;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Videos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="videos-view">

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id_videos], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_videos',
            'id_product',
            'id_model',
            'highlight:ntext',
            'video_value:ntext',
            'id_author:ntext',
            'image:ntext',
            'video_title',
            'link_rewrite',
            'add_date',
            'update_date',
            'created_by_id',
            'updated_by_id',
            'view_count',
            'status',
            'video_caption',
        ],
    ]) ?>

</div>
