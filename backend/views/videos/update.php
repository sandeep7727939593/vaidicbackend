<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Videos */

$this->title = Yii::t('app', 'Update Videos: {name}', [
    'name' => $model->id_videos,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Videos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_videos, 'url' => ['view', 'id' => $model->id_videos]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="videos-update">

    <?= $this->render('_form', [
        'model' => $model,
        'productList' => $productList,
        'authorList' => $authorList,
        'modelList' => $modelList,
    ]) ?>

</div>
