<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Product;
use common\models\Model;
/* @var $this yii\web\View */
/* @var $searchModel common\models\search\VideosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Videos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="videos-index">

    <p>
        <?= Html::a(Yii::t('app', 'Create Videos'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            // ['class' => 'yii\grid\SerialColumn'],

            'id_videos',
            [
                'attribute' => 'id_product',
                'label' => 'Product Name',
                'filter' => Product::dropdown(),
                 'value' => function($model, $index, $dataColumn) {
                        return $model->product->title;
                    },            
            ],
            [
                'attribute' => 'id_model',
                'label' => 'Model Name',
                'filter' => Model::dropdown(),
                 'value' => function($model, $index, $dataColumn) {
                        return $model->model->model_name;
                    },            
            ],
            // 'highlight:ntext',
            // 'video_value:ntext',
            //'id_author:ntext',
            //'image:ntext',
            'video_title',
            'link_rewrite',
            //'add_date',
            //'update_date',
            //'created_by_id',
            //'updated_by_id',
            //'view_count',
            'status',
            //'video_caption',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view}{update}'],
        ],
    ]); ?>


</div>
