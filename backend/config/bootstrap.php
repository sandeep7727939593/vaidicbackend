<?php
Yii::setAlias('webroot', dirname(__DIR__) . '/web');
Yii::setAlias('web', '');
$container = Yii::$container;
$container->set('frontend\services\BaseServiceInterface', [
    'class' => 'frontend\services\BaseServices',
]);
$container->set('frontend\services\CategoryServiceInterface', [
    'class' => 'frontend\services\CategoryServices',
]);