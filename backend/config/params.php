<?php
return [
    'clientOptions' => [
        'plugins' => [
            'print preview image code media searchreplace autolink directionality template table powerpaste textcolor wordcount textpattern link lists advlist emoticons pagebreak anchor'
        ],
        'style_formats' => [
            [ 
                'title' => 'Red Title',
                'inline' => 'span',
                'styles' => [ 
                    'color' => 'red' 
                ],
            ],
            [ 
                'title' => 'Blue Title',
                'inline' => 'span',
                'styles' => [ 
                    'color' => 'blue' 
                ],
            ],
            [ 
                'title' => 'Marker: Yellow',
                'inline' => 'span',
                'styles' => [ 
                    'background-color' => 'yellow' 
                ],
            ],
            [ 
                'title' => 'Marker: Green',
                'inline' => 'span',
                'styles' => [ 
                    'background-color' => 'green' 
                ],
            ],
            [ 
                'title' => 'Big',
                'inline' => 'big',                    
            ],
            [ 
                'title' => 'Small',
                'inline' => 'small',                    
            ],
            [ 
                'title' => 'Typewriter',
                'inline' => 'tt',                    
            ],
            [ 
                'title' => 'Computer Code',
                'inline' => 'code',                    
            ],
            [ 
                'title' => 'Keyboard Phrase',
                'inline' => 'kbd',                    
            ],
            [ 
                'title' => 'Sample Text',
                'inline' => 'samp',                    
            ],
            [ 
                'title' => 'Keyboard Phrase',
                'inline' => 'kbd',                    
            ],
            [ 
                'title' => 'Variable',
                'inline' => 'var',                    
            ],
            [ 
                'title' => 'Deleted Text',
                'inline' => 'del',                    
            ],
            [ 
                'title' => 'Inserted Text',
                'inline' => 'ins',                    
            ],
            [ 
                'title' => 'Cited Work',
                'inline' => 'cite',                    
            ],
            [ 
                'title' => 'Inline Quotation',
                'inline' => 'q',                    
            ],
        ],
        'images_upload_url' => '',
        'automatic_uploads' => true,
        'images_reuse_filename' => false,
        'images_upload_base_path' => '',
        'file_picker_types' => 'file image media',
        'images_upload_credentials' => false,
        'image_advtab' => true,
        'media_live_embeds'=> true, 
        'toolbar' => 'undo redo | styleselect | formatselect | fontselect | fontsizeselect | bold italic underline strikethrough forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | image media | code | removeformat | cut copy paste | table | link | emoticons | pagebreak | blockquote | anchor',
        'link_assume_external_targets' => true,
        'default_link_target' => "_blank",
        'file_picker_callback'=> new yii\web\JsExpression("function(cb, value, meta) {
          
          if (meta.filetype == 'media') {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'media/*');
            // File selection
            input.onchange = function() {                  
              var file = this.files[0];
              var reader = new FileReader();

              // FormData
              var fd = new FormData();
              var files = file;
              fd.append('file',files);
              fd.append('filetype',meta.filetype);
              var filename = '';
              
              // AJAX
              jQuery.ajax({
                  url: '/common/upload-editor-media',
                  type: 'POST',
                  data: fd,
                  contentType: false,
                  processData: false,
                  async: false,
                  success: function(response){
                    if(response.path){
                      filename = response;
                    }
                    else {
                      alert(response.message);
                      $('.mce-close').trigger('click');
                    }
                  }
              });
              reader.onload = function(e) {
                cb(filename.path);
              };
              reader.readAsDataURL(file);
            };
            input.click();
          }
        }"),
        
    ]
];
