<?php
$env = ENVIRONMENT;
$params = array_merge(
    require (__DIR__ . '/../../common/config/params.php'),
    require (__DIR__ . "/../../common/config/params-$env.php"),
    require (__DIR__ . '/params.php'),
    require (__DIR__ . "/params-$env.php")
);
$config = [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'admin' => [
            'class' => 'mdm\admin\Module',
            'controllerMap' => [
               'assignment' => [
                   'class' => 'mdm\admin\controllers\AssignmentController',
                   'userClassName' => 'common\models\Users',
                   'idField' => 'id',
                   'usernameField' => 'username',
                   'fullnameField' => 'username',
                   'extraColumns' => ['email'],
                   'searchClass' => 'common\models\search\UsersSearch'
               ],
            ],
        ],
        'gridview' =>  [
            'class' => '\kartik\grid\Module',
            // your other grid module settings
        ],
        'gridviewKrajee' =>  [
            'class' => '\kartik\grid\Module',
            // your other grid module settings
        ]
    ],
    'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            'site/*', // add or remove allowed actions to this list
            'admin/*',
        ]
    ],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager', // or use 'yii\rbac\DbManager'
            'defaultRoles' => ['guest', 'user'],
        ],
        'request' => [
            // 'csrfParam' => '_csrf-backend',
            'cookieValidationKey' => 'backend',
            // 'enableCsrfValidation' => false,
        ],
        'user' => [
            'identityClass' => 'common\models\Users',
            'enableAutoLogin' => false,
            'loginUrl' => ['/site/login'],
            'authTimeout' => 86400,
            // 'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                "site/login" => "site/login",
            ]
        ],
        // 'authClientCollection' => [
        //     'class' => 'yii\authclient\Collection',
        //     'clients' => [
        //         'google' => [
        //             'class' => 'yii\authclient\clients\Google',
        //             'clientId' => '22565399099-kpq443ckbpka67ghdanjonngjc3hcc55.apps.googleusercontent.com',
        //             'clientSecret' => 'Js6VJHEn1Rp55fEZiSvf4OTl',
        //         ],
        //     ],
        // ],
        'view' => [
             'theme' => [
                 'pathMap' => [
                    '@app/views' => '@backend/themes/admin'
                 ],
             ],
        ],
        'image' => [
            'class' => 'backend\components\ImageComponent',
        ],
    ],
    'params' => $params,
];

$config['bootstrap'] = ['gii'];
$config['modules']['gii'] = [
    'class' => 'yii\gii\Module'
];


return $config;