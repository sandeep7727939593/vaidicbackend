<?php
namespace backend\components;

// use common\models\Auth;
use Yii;
use yii\base\Component;
use common\helpers\Common;
/**
 * AuthHandler handles successful authentification via Yii auth component
 */
class ImageComponent extends Component
{
    public $allowedImageWidth = 650;
    public $allowedImageHeight = 420;

    public function upload($img, $path)
    {
        $basicPath = Yii::$app->params['BASIC_IMAGE_UPLOAD_PATH'];
        $fullPath = $basicPath . $path;
        if(!file_exists($fullPath)) {
            mkdir($fullPath, 0777, true);
        }
        $url = Common::getUrlGenerater($img->baseName);
        $url = $url . '.' . $img->extension;
        $img->saveAs($fullPath . $url);
        return $url;
    }

    public function videoUpload($video, $path)
    {
        $basicPath = Yii::$app->params['BASIC_VIDEO_UPLOAD_PATH'];
        $fullPath = $basicPath . $path;
        if(!file_exists($fullPath)) {
            mkdir($fullPath, 0777, true);
        }
        $url = Common::getUrlGenerater($video->baseName);
        $url = $url . '.' . $video->extension;
        $video->saveAs($fullPath . $url);
        return $url;
    }

}