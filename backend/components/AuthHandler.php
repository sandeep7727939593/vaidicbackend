<?php
namespace backend\components;

// use common\models\Auth;
use common\models\Users;
use Yii;
use yii\authclient\ClientInterface;
use yii\helpers\ArrayHelper;

/**
 * AuthHandler handles successful authentification via Yii auth component
 */
class AuthHandler
{
    /**
     * @var ClientInterface
     */
    private $client;

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    public function handle()
    {
        $attributes = $this->client->api('userinfo', 'GET');
        $email = ArrayHelper::getValue($attributes, 'email');
        $id = ArrayHelper::getValue($attributes, 'id');
        $nickname = ArrayHelper::getValue($attributes, 'name');

        /* @var Auth $auth */
        $auth = Users::find()->where([
            'email' => $email,
            'is_admin' => 1,
            'status' => 1
        ])->one();

        if ($auth) { // login
            
            Yii::$app->user->login($auth, 86400);            
            Yii::$app->getSession()->setFlash('success', [
                Yii::t('app', 'Successfully Login.', [
                    'client' => $this->client->getTitle()
                ]),
            ]);
            
        } else { // signup
            if ($email !== null && Users::find()->where(['email' => $email])->exists()) {
                Yii::$app->getSession()->setFlash('error', [
                    Yii::t('app', "Users with the same email as in {client} account already exists but isn't Admin to it. Contact backend administrator.", ['client' => $this->client->getTitle()]),
                ]);
            } else {
                $password = Yii::$app->security->generateRandomString(16);
                
                $users = new Users([
                    'username' => $nickname,
                    'email' => $email,
                    'password' => $password,
                    'is_admin' => 1,
                    'status' => 1
                ]);

                $users->generateAuthKey();

                $transaction = Users::getDb()->beginTransaction();

                if ($users->save()) {
                    
                    $transaction->commit();
                    Yii::$app->user->login($users,86400);

                    Yii::$app->getSession()->setFlash('success', [
                        Yii::t('app', 'Successfully Login.', [
                            'client' => $this->client->getTitle()
                        ]),
                    ]);
                    
                } else {
                    Yii::$app->getSession()->setFlash('error', [
                        Yii::t('app', 'Unable to save Users: {errors}', [
                            'client' => $this->client->getTitle(),
                            'errors' => json_encode($users->getErrors()),
                        ]),
                    ]);
                }
            }
        }
    }
}