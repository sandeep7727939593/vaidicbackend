<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this \yii\web\View */
/* @var $content string */
?>

<header class="main-header">
    
    <?= Html::a('<span class="logo-mini">SD</span><span class="logo-lg">' . Yii::$app->params['name'] . '</span>', Yii::$app->params['homeUrl'], ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only"><?php echo Yii::$app->params['siteName']; ?> Navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">                

                <li class="dropdown user user-menu">
                    <a href="javascript:0">
                        <span class="hidden-xs"><?= Yii::$app->user->identity->username ?></span>
                    </a>
                </li>
                <li>
                    <a href="<?= Url::toRoute('/site/logout', true); ?>"><i class="fa fa-power-off"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>
