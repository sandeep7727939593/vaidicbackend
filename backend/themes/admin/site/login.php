<?php
use yii\helpers\Html;
use dmstr\widgets\Alert;
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Sign In';

?>

<div class="login-box">
    <div class="login-logo">
        <a href="#"><b>Backend</b>&nbsp;Admin</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <center>
        <?= yii\authclient\widgets\AuthChoice::widget([
             'baseAuthUrl' => ['site/auth'],
             'popupMode' => false,
        ]) ?>
        </center>
    </div>
    <section class="content">
        <?= Alert::widget() ?>
    </section>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
