<?php

namespace common\helpers;

use Yii;

class Common {

	public static function sendSms($to, $body)
	{
        $fields = array(
            "sender_id" => "FSTSMS",
            "message" => $body,
            "language" => "english",
            "route" => "p",
            "numbers" => $to,
            "flash" => "1"
        );

        $curl = curl_init();

        curl_setopt_array($curl, array(
	        CURLOPT_URL => "https://www.fast2sms.com/dev/bulk",
	        CURLOPT_RETURNTRANSFER => true,
	        CURLOPT_ENCODING => "",
	        CURLOPT_MAXREDIRS => 10,
	        CURLOPT_TIMEOUT => 30,
	        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	        CURLOPT_CUSTOMREQUEST => "POST",
	        CURLOPT_POSTFIELDS => json_encode($fields),
	        CURLOPT_HTTPHEADER => array(
	            "authorization:tsP2nVI4Yw7Sy5fDBNXET3Hgz9xe1oOQFdUjqvkclh0JRA8ura60uynSU2EOvH89q3zNBxmVbKJZTYIl",
	            "accept: */*",
	            "cache-control: no-cache",
	            "content-type: application/json"
	        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          return "cURL Error #:" . $err;
        } else {
          return $response;
        }
	}

    public static function getCategoryUrl($category, $queryString = '', $fullurl = true) {
        $queryString = !empty($queryString) ? '?topic=' . $queryString : '';
        return ($fullurl ? Yii::$app->params['baseUrl'] : '/') . $category . '.html' . $queryString;
    }

    public function getUrlGenerater($string='')
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.

        $string = preg_replace('/-+/', '-', $string); // Replaces multiple hyphens with single one
        return strtolower($string);
    }

}

?>