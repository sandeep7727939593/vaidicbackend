<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
    'twilioSid' => 'AC4dd7959144b8e105bbc21a185e2f740b', //replace with your sid
    'twiliotoken' => 'afcd375cb5b8b175e6d61dfa6c6e2617', //replace with your token
    'twilioNumber'=>'+917727939593',//replace with your Twilio phone number

    'fastSmsApi' => 'https://www.fast2sms.com/dev/bulk',
    'cdnUrl' => 'http://local.yii.com/',
    'baseUrl' => 'http://local.yii.com/',
    'name' => 'Backend',
    'homeUrl' => 'http://local.backend.com',
    'siteName' => 'Backend',
];
