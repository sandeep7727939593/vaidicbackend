<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
$container = Yii::$container;
$container->set('common\models\dao\BaseDaoInterface', [
    'class' => 'common\models\dao\BaseDao',
]);
$container->set('common\models\dao\CategoryDaoInterface', [
    'class' => 'common\models\dao\CategoryDao',
]);
$container->set('common\models\dao\TopicDaoInterface', [
    'class' => 'common\models\dao\TopicDao',
]);