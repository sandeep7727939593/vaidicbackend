<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\AttributeBehavior;
use yii\web\IdentityInterface;
/**
 * This is the model class for table "brand".
 *
 * @category BikeDekho
 * @package  DeviceInfo
 * @author   Vishnu Soni <vishnu.soni@girnarsoft.com>
 * @license  http://girnarsoft.com Girnarsoft
 * @link     http://girnarsoft.com
 * @property string    $id
 * @property string    $username
 * @property string    $password
 * @property string    $email
 * @property string    $emailEnc
 * @property string    $activkey
 * @property string    $createtime
 * @property string    $lastvisit
 * @property string    $superuser
 * @property string    $status
 * @property string    $is_admin
 * @property string    $created_by_id
 * @property string    $updated_by_id
 * @property string    $source
 * @property string    $and_dwn_sms
 * @property string    $is_subscribe
 */
class Users extends ActiveRecord implements IdentityInterface
{

    /**
     * Behaviours for updated_by_name,created_by_name
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'createtime',
                ],
                'value' => function ($event) {
            return time();
        },
            ],
        ];
    }

    /**

     * tableName
     *
     * @return array
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * tableName
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['username', 'email'], 'required'],
            [['username', 'password', 'email', 'emailEnc', 'activkey', 'createtime', 'created_by_id', 'updated_by_id', 'and_dwn_sms', 'source'], 'safe'],
            [['status'], 'default', 'value' => '0'],
            [['is_subscribe'], 'default', 'value' => '1'],
            //[['is_sync'], 'default', 'value' => '0'],
            [['email'], 'unique'],
            //[['username'], 'unique'],
            [['is_admin'], 'default', 'value' => '0']
        ];
    }

   

    /**
     * tableName
     *
     * @return array
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'id']);
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {        
        return static::findOne(['activkey' => $token]);
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->activkey;
        //return $this->hmac_shopify;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function generateAuthKey()
    {
        $this->activkey = Yii::$app->security->generateRandomString();
    }
}
