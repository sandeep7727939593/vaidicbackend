<?php

namespace common\models;

use Yii;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "{{%community}}".
 *
 * @property int $id_post
 * @property string|null $type
 * @property string|null $description
 * @property int|null $status
 * @property string|null $add_date
 * @property string|null $launch_date
 * @property string $update_date
 * @property string|null $created_by_id
 * @property string|null $updated_by_id
 */
class Community extends \yii\db\ActiveRecord
{
    /**
     * Behaviours for updated_date,created_date,add_date,logger behaviour
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['add_date','update_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'update_date',
                ],
                'value' => function ($event) {                    
                    return date('Y-m-d H:i:s');                    
                },                
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%community}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type','description'], 'required'],
            [['description'], 'string'],
            [['status'], 'integer'],
            [['add_date', 'launch_date', 'update_date'], 'safe'],
            [['type'], 'string', 'max' => 10],
            [['created_by_id', 'updated_by_id'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_post' => Yii::t('app', 'Id Post'),
            'type' => Yii::t('app', 'Type'),
            'description' => Yii::t('app', 'Embedded Content'),
            'status' => Yii::t('app', 'Status'),
            'add_date' => Yii::t('app', 'Add Date'),
            'launch_date' => Yii::t('app', 'Launch Date'),
            'update_date' => Yii::t('app', 'Update Date'),
            'created_by_id' => Yii::t('app', 'Created By ID'),
            'updated_by_id' => Yii::t('app', 'Updated By ID'),
        ];
    }
}