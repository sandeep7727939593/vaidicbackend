<?php

namespace common\models;

use Yii;
use common\helpers\Common;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "{{%product}}".
 *
 * @property int $id_product
 * @property string|null $title
 * @property string|null $sub_title
 * @property string|null $description
 * @property string|null $image
 * @property string|null $link_rewrite
 * @property string|null $add_date
 * @property string $update_date
 * @property string|null $created_by_id
 * @property string|null $updated_by_id
 * @property int|null $status
 */
class Product extends \yii\db\ActiveRecord
{

    public $file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%product}}';
    }
    /**
     * Behaviours for updated_date,created_date,add_date,logger behaviour
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['add_date','update_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'update_date',
                ],
                'value' => function ($event) {                    
                    return date('Y-m-d H:i:s');                    
                },                
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description','title','sub_title'], 'required'],
            [['description'], 'string'],
            [['add_date', 'update_date'], 'safe'],
            [['status', 'id_product'], 'integer'],
            [['title', 'link_rewrite', 'created_by_id', 'updated_by_id'], 'string', 'max' => 50],
            [['sub_title', 'image'], 'string', 'max' => 500],
            [['file'], 'image', 'extensions' => ['jpg', 'jepg', 'png']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_product' => Yii::t('app', 'Id Proctuct'),
            'title' => Yii::t('app', 'Title'),
            'sub_title' => Yii::t('app', 'Sub Title'),
            'description' => Yii::t('app', 'Description'),
            'image' => Yii::t('app', 'Image'),
            'file' => Yii::t('app', 'Image'),
            'link_rewrite' => Yii::t('app', 'Link Rewrite'),
            'add_date' => Yii::t('app', 'Add Date'),
            'update_date' => Yii::t('app', 'Update Date'),
            'created_by_id' => Yii::t('app', 'Created By ID'),
            'updated_by_id' => Yii::t('app', 'Updated By ID'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $this->link_rewrite = Common::getUrlGenerater($this->title);
        }
        return parent::beforeSave($insert);
    }

    public static function dropdown()
    {
        $dropdown = ['' => 'Search Product'];
        $product = Product::find()->asArray()->all();
        foreach ($product as $p) {
            $dropdown[$p['id_product']] = $p['title'];
        }
        return $dropdown;
    }

}