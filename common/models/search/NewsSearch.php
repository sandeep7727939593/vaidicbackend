<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\News;

/**
 * NewsSearch represents the model behind the search form of `common\models\News`.
 */
class NewsSearch extends News
{
    public $product_name;
    public $modelname;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_news', 'id_product', 'id_model', 'created_by_id', 'updated_by_id', 'view_count', 'status'], 'integer'],
            [['content', 'id_author', 'image', 'title', 'link_rewrite', 'add_date', 'update_date', 'product_name', 'modelname'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = News::find();
         $query->joinWith(['product', 'model']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_news' => $this->id_news,
            'news.id_product' => $this->id_product,
            'news.id_model' => $this->id_model,
            'news.status' => $this->status
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'news.link_rewrite', $this->link_rewrite]);
        $query->orderBy('id_news DESC');
        return $dataProvider;
    }
}