<?php

namespace common\models\search;

// use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Model;

/**
 * ModelSearch represents the model behind the search form of `common\models\Model`.
 */
class ModelSearch extends Model
{
    public $product_name;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_model', 'id_product', 'base_price', 'popularity', 'status'], 'integer'],
            [['cover_image', 'model_name', 'short_name', 'link_rewrite', 'description', 'alternative_link_rewrite', 'alternative_link_type', 'add_date', 'launch_date', 'update_date', 'created_by_id', 'updated_by_id', 'product_name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Model::find();

        $query->joinWith(['product']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_model' => $this->id_model,
            'product.id_product' => $this->id_product,
        ]);

        $query->andFilterWhere(['like', 'product_name', $this->product_name])
            ->andFilterWhere(['like', 'model_name', $this->model_name])
            ->andFilterWhere(['like', 'link_rewrite', $this->link_rewrite]);
        $query->orderBy('id_model DESC');

        return $dataProvider;
    }
}