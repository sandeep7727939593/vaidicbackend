<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Videos;

/**
 * VideosSearch represents the model behind the search form of `common\models\Videos`.
 */
class VideosSearch extends Videos
{
    public $product_name;
    public $modelname;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_videos', 'id_product', 'id_model', 'created_by_id', 'updated_by_id', 'view_count', 'status'], 'integer'],
            [['highlight', 'video_value', 'id_author', 'image', 'video_title', 'link_rewrite', 'add_date', 'update_date', 'video_caption', 'product_name', 'modelname'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Videos::find();
        $query->joinWith(['product', 'model']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_videos' => $this->id_videos,
            'videos.id_product' => $this->id_product,
            'videos.id_model' => $this->id_model,
            'videos.status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'video_title', $this->video_title])
            ->andFilterWhere(['like', 'videos.link_rewrite', $this->link_rewrite])
            ->andFilterWhere(['like', 'product_name', $this->product_name])
            ->andFilterWhere(['like', 'modelname', $this->modelname]);
        $query->orderBy('id_videos DESC');
        return $dataProvider;
    }
}