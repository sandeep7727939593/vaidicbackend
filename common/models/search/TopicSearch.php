<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Topic;

/**
 * Topic represents the model behind the search form of `common\models\Topic`.
 */
class TopicSearch extends Topic
{
    public $category;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_topic', 'id_category', 'status', 'index_position'], 'integer'],
            [['title', 'link_rewrite', 'side_menu_title', 'description', 'created_by_id', 'updated_by_id', 'add_date', 'update_date', 'category'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Topic::find();

        $query->joinWith(['category']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_topic' => $this->id_topic,
            'topic.status' => $this->status,
            'index_position' => $this->index_position,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'side_menu_title', $this->side_menu_title])
            ->andFilterWhere(['like', 'category.category', $this->category]);

        return $dataProvider;
    }
}