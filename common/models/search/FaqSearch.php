<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Faq;

/**
 * FaqSearch represents the model behind the search form of `common\models\Faq`.
 */
class FaqSearch extends Faq
{
    public $product_name;
    public $modelname;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_faq', 'id_product', 'id_model', 'status'], 'integer'],
            [['link_rewrite', 'question', 'answer', 'add_date', 'launch_date', 'update_date', 'created_by_id', 'updated_by_id', 'product_name', 'modelname'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Faq::find();
         $query->joinWith(['product', 'model']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_faq' => $this->id_faq,
            'faq.id_product' => $this->id_product,
            'faq.id_model' => $this->id_model,
            'faq.status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'link_rewrite', $this->link_rewrite])
            ->andFilterWhere(['like', 'question', $this->question])
            ->andFilterWhere(['like', 'answer', $this->answer])
            ->orderBy('id_faq DESC');

        return $dataProvider;
    }
}