<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Benefit;

/**
 * BenefitSearch represents the model behind the search form of `common\models\Benefit`.
 */
class BenefitSearch extends Benefit
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_benefit', 'id_product', 'id_model', 'status'], 'integer'],
            [['value', 'add_date', 'launch_date', 'update_date', 'created_by_id', 'updated_by_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Benefit::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_benefit' => $this->id_benefit,
            'benefit.id_product' => $this->id_product,
            'benefit.id_model' => $this->id_model,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'value', $this->value]);

        return $dataProvider;
    }
}