<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Community;

/**
 * CommunitySearch represents the model behind the search form of `common\models\Community`.
 */
class CommunitySearch extends Community
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_post', 'status'], 'integer'],
            [['type', 'description', 'add_date', 'launch_date', 'update_date', 'created_by_id', 'updated_by_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Community::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_post' => $this->id_post,
            'status' => $this->status,
            'add_date' => $this->add_date,
            'launch_date' => $this->launch_date,
            'update_date' => $this->update_date,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'created_by_id', $this->created_by_id])
            ->andFilterWhere(['like', 'updated_by_id', $this->updated_by_id]);

        return $dataProvider;
    }
}
