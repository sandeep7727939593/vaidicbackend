<?php

namespace common\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Users;

/**
 * UserSearch represents the model behind the search form of `common\models\User`.
 */
class UsersSearch extends Users
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'createtime', 'lastvisit', 'superuser', 'status', 'is_admin', 'created_by_id', 'updated_by_id'], 'integer'],
            [['username', 'password', 'email', 'activkey', 'source', 'and_dwn_sms', 'is_subscribe'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Users::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => 1,
            'is_admin' => 1,
            'createtime' => $this->createtime,
            'lastvisit' => $this->lastvisit,
            'superuser' => $this->superuser,
            
            'created_by_id' => $this->created_by_id,
            'updated_by_id' => $this->updated_by_id,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'activkey', $this->activkey])
            ->andFilterWhere(['like', 'source', $this->source])
            ->andFilterWhere(['like', 'and_dwn_sms', $this->and_dwn_sms])
            ->andFilterWhere(['like', 'is_subscribe', $this->is_subscribe]);

        return $dataProvider;
    }
}