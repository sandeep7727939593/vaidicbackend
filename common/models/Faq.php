<?php

namespace common\models;

use Yii;
use common\helpers\Common;
use common\models\Product;
use common\models\Model;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
/**

/**
 * This is the model class for table "{{%faq}}".
 *
 * @property int $id_faq
 * @property int|null $id_product
 * @property int|null $id_model
 * @property string|null $link_rewrite
 * @property string|null $question
 * @property string|null $answer
 * @property int|null $status
 * @property string|null $add_date
 * @property string|null $launch_date
 * @property string $update_date
 * @property string|null $created_by_id
 * @property string|null $updated_by_id
 */
class Faq extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%faq}}';
    }

    /**
     * Behaviours for updated_date,created_date,add_date,logger behaviour
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['add_date','update_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'update_date',
                ],
                'value' => function ($event) {                    
                    return date('Y-m-d H:i:s');                    
                },                
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['question', 'answer'], 'required'],
            [['id_product', 'status'], 'integer'],
            [['answer', 'id_model'], 'string'],
            [['add_date', 'launch_date', 'update_date'], 'safe'],
            [['link_rewrite', 'created_by_id', 'updated_by_id'], 'string', 'max' => 50],
            [['question'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_faq' => Yii::t('app', 'Id Faq'),
            'id_product' => Yii::t('app', 'Product'),
            'id_model' => Yii::t('app', 'Model'),
            'link_rewrite' => Yii::t('app', 'Link Rewrite'),
            'question' => Yii::t('app', 'Question'),
            'answer' => Yii::t('app', 'Answer'),
            'status' => Yii::t('app', 'Status'),
            'add_date' => Yii::t('app', 'Add Date'),
            'launch_date' => Yii::t('app', 'Launch Date'),
            'update_date' => Yii::t('app', 'Update Date'),
            'created_by_id' => Yii::t('app', 'Created By ID'),
            'updated_by_id' => Yii::t('app', 'Updated By ID'),
        ];
    }

    // public function beforeSave($insert) {
    //     if ($this->isNewRecord) {
    //         $this->link_rewrite = Common::getUrlGenerater($this->question);
    //     }
    //     return parent::beforeSave($insert);
    // }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id_product' => 'id_product']);
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(Model::className(), ['id_model' => 'id_model']);
    }
}