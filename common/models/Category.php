<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id_category
 * @property string $category
 * @property string $link_rewrite
 * @property int|null $status
 * @property string|null $created_by_id
 * @property string|null $updated_by_id
 * @property string|null $add_date
 * @property string $update_date
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category', 'link_rewrite'], 'required'],
            [['status'], 'integer'],
            [['add_date', 'update_date'], 'safe'],
            [['category'], 'string', 'max' => 100],
            [['link_rewrite'], 'string', 'max' => 500],
            [['created_by_id', 'updated_by_id'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_category' => 'Id Category',
            'category' => 'Category',
            'link_rewrite' => 'Link Rewrite',
            'status' => 'Status',
            'created_by_id' => 'Created By ID',
            'updated_by_id' => 'Updated By ID',
            'add_date' => 'Add Date',
            'update_date' => 'Update Date',
        ];
    }
}
