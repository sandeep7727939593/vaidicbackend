<?php

namespace common\models\dao;

use Yii;
use common\models\dao\TopicDaoInterface;
use common\models\dao\BaseDao;
use common\models\Topic;

/**
 * 
 */
class TopicDao extends BaseDao implements TopicDaoInterface
{
	public $modelClass = 'common\models\Topic';
}


?>