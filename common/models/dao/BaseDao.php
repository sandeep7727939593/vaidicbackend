<?php

namespace common\models\dao;

use common\models\dao\BaseDaoInterface;

class BaseDao implements BaseDaoInterface {

/**
     * Count number of records for a filter condition
     *
     * @param array $condition condition
     * @param array $params    params
     *
     * @return integer
     */
    public function count($condition = [], $params = [])
    {
        $modelQuery = $this->findCriteria([], $condition, $params);
        return $modelQuery->count();
    }

    /**
     * Get Model class method
     *
     * @param mixed $primaryKey value of primaryKey of table
     *
     * @return User|static
     */
    public function getModel($primaryKey = null)
    {
        $modelClass = $this->modelClass;
        $primaryKeyAttr = $modelClass::primaryKey();
        if (!is_null($primaryKey)) {
            $model = $modelClass::findOne([
                $primaryKeyAttr[0] => $primaryKey
            ]);
            if (!is_null($model)) {
                return $model;
            }
        }
        return new $modelClass;
    }

    /**
     * user create method
     *
     * @param ActiveRecord $model model
     *
     * @return bool|mixed
     */
    public function create($model, $runValidation = true)
    {
        return $model->save($runValidation);
    }

    /**
     * Model update method
     *
     * @param ActiveRecord $model model
     *
     * @return mixed
     */
    public function update($model)
    {
        return $model->save();
    }

    /**
     * delete method
     *
     * @param array $condition condition
     *
     * @return mixed|void
     */
    public function delete($condition = [], $params = [])
    {
        $modelClass = $this->modelClass;
        $modelQuery = $modelClass::deleteAll($condition, $params);
        return $modelQuery;
    }

    /**
     * Update method
     *
     * @param array $attributes attributes
     * @param array $condition  condition
     * @param array $params     params
     *
     * @return integer the number of rows updated
     */
    public function updateAll($attributes, $condition = [], $params = [])
    {
        $modelClass = $this->modelClass;
        return $modelClass::updateAll($attributes, $condition, $params);
    }

    /**
     * User select method
     *
     * @param array $fields    fields
     * @param array $condition condition
     * @param array $params    params
     *
     * @return array|mixed|\yii\db\ActiveRecord[]
     */
    public function selectAll($fields = [], $condition = [], $params = [])
    {
        $modelQuery = $this->findCriteria($fields, $condition, $params);
        return $modelQuery->all();
    }

    /**
     * User select mehtod
     *
     * @param array $fields    fields
     * @param array $condition condition
     * @param array $params    params
     *
     * @return array|mixed|\yii\db\ActiveRecord[]
     */
    public function select($fields = [], $condition = [], $params = [])
    {
        $modelQuery = $this->findCriteria($fields, $condition, $params);
        return $modelQuery->one();
    }

    /**
     * User select mehtod
     *
     * @param array $fields    fields
     * @param array $condition condition
     * @param array $params    params
     *
     * @return array|mixed|\yii\db\ActiveRecord[]
     */
    public function selectAllDataAsArray($fields, $condition, $params)
    {
        $modelQuery = $this->findCriteria($fields, $condition, $params);
        //echo $modelQuery->createCommand()->getRawSql() . "<br>";
        return $modelQuery->asArray()->all();
    }

    /**
     * User select mehtod
     *
     * @param array $fields    fields
     * @param array $condition condition
     * @param array $params    params
     *
     * @return array|mixed|\yii\db\ActiveRecord[]
     */
    public function selectAsArray($fields, $condition, $params)
    {
        $modelQuery = $this->findCriteria($fields, $condition, $params);
        return $modelQuery->asArray()->one();
    }

    /**
     * find Criteria
     *
     * @param array $fields    fields
     * @param array $condition condition
     * @param array $params    params
     *
     * @return array|mixed|\yii\db\ActiveRecord[]
     */
    public function findCriteriaDataAsArray($fields = [], $condition = [], $params = [])
    {
        $modelClass = $this->modelClass;
        $modelQuery = $modelClass::find();
        $modelQuery->select($fields);
        foreach ($condition as $conditionAttribute => $attributeValue) {
            $modelQuery->andFilterWhere(['=', $conditionAttribute, $attributeValue]);
        }
        if (isset($params['order'])) {
            $modelQuery->orderBy($params['order']);
        }
        if (isset($params['limit']) && $params['limit'] != -1) {
            $modelQuery->limit($params['limit']);
            $modelQuery->offset($params['limit'] * ($params['page'] - 1));
        }
        if (isset($params['group']) && !empty($params['group'])) {
            $modelQuery->groupBy($params['group']);
        }
        if (isset($params['join']) && count($params['join'])) {
            // here not using method so that multiple join can be applied
            $modelQuery->join = $params['join'];
        }
        return $modelQuery;
    }

    /**
     * add source filter in condition
     *
     * @param $modelQuery
     */
    public function addSourceFilter(&$modelQuery, $attributeName = "source")
    {
        $source = Common::getSourceFilter();
        $modelQuery->andWhere([$attributeName => $source]);
    }


    /**
     * This function add the common data filters to the query object
     * @param [type] &$modelQuery [description]
     * @param [type] $params      [description]
     */
    public function addCommonFilters(&$modelQuery, $params) {
        if (isset($params['order'])) {
            $modelQuery->orderBy($params['order']);
        }
        if (isset($params['limit']) && $params['limit'] != -1) {
            $modelQuery->limit($params['limit']);
            $modelQuery->offset($params['limit'] * ($params['page'] - 1));
        }
        if (isset($params['group']) && !empty($params['group'])) {
            $modelQuery->groupBy($params['group']);
        }
        if (isset($params['join']) && count($params['join'])) {
            $modelQuery->join = $params['join'];
        }
    }

    /**
     * This function is generic to add filter condition
     * @param [type] &$modelQuery [description]
     * @param [type] $condition   [description]
     */
    public function addFilterCondition(&$modelQuery, $condition) {
        foreach ($condition as $conditionAttribute => $attributeValue) {
            if ($conditionAttribute == "mixed_condition" && is_array($attributeValue)) {
                foreach ($attributeValue as $value) {
                    if (is_array($value)) {
                        $modelQuery->andFilterWhere($value);
                    } else {
                        $modelQuery->andWhere($value);
                    }
                }
            } else {
                $modelQuery->andFilterWhere(['=', $conditionAttribute, $attributeValue]);
            }
        }
    }

    /**
     * find Criteria
     *
     * @param array $fields    fields
     * @param array $condition condition
     * @param array $params    params
     *
     * @return array|mixed|\yii\db\ActiveRecord[]
     */
    public function findCriteria($fields = [], $condition = [], $params = [])
    {
        $modelClass = $this->modelClass;
        $modelQuery = $modelClass::find();
        $modelQuery->select($fields);
        $this->addFilterCondition($modelQuery, $condition);
        $this->addCommonFilters($modelQuery, $params);
        return $modelQuery;
    }
	
}
?>