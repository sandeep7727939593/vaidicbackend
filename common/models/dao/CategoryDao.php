<?php

namespace common\models\dao;

use Yii;
use common\models\dao\CategoryDaoInterface;
use common\models\dao\BaseDao;
use common\models\Category;

/**
 * 
 */
class CategoryDao extends BaseDao implements CategoryDaoInterface
{
	public $modelClass = 'common\models\Category';
}


?>