<?php

namespace common\models;

use Yii;
use common\helpers\Common;
use common\models\Model;
use common\models\Product;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%videos}}".
 *
 * @property int $id_videos
 * @property int $id_product
 * @property int $id_model
 * @property string|null $highlight
 * @property string|null $video_value
 * @property string|null $id_author
 * @property string|null $image
 * @property string|null $video_title
 * @property string|null $link_rewrite
 * @property string $add_date
 * @property string $update_date
 * @property int|null $created_by_id
 * @property int|null $updated_by_id
 * @property int|null $view_count
 * @property int|null $status
 * @property string|null $video_caption
 */
class Videos extends \yii\db\ActiveRecord
{
    public $videoUpload;
    public $coverImage;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%videos}}';
    }

     /**
     * Behaviours for updated_date,created_date,add_date,logger behaviour
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['add_date','update_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'update_date',
                ],
                'value' => function ($event) {                    
                    return date('Y-m-d H:i:s');                    
                },                
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_product', 'id_model', 'video_title', 'id_author', 'video_value', 'image'], 'required'],
            [['id_product', 'id_model', 'created_by_id', 'updated_by_id', 'view_count', 'status'], 'integer'],
            [['highlight', 'video_value', 'id_author', 'image'], 'string'],
            [['add_date', 'update_date'], 'safe'],
            [['video_title', 'link_rewrite', 'video_caption'], 'string', 'max' => 500],
            [['coverImage'], 'image', 'extensions' => ['jpg', 'jepg', 'png']],
            [['videoUpload'], 'file' , 'extensions' => ['mp4']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_videos' => Yii::t('app', 'Id Videos'),
            'id_product' => Yii::t('app', 'Product Name'),
            'id_model' => Yii::t('app', 'Model Name'),
            'highlight' => Yii::t('app', 'Highlight'),
            'video_value' => Yii::t('app', 'Video'),
            'id_author' => Yii::t('app', 'Author Name'),
            'image' => Yii::t('app', 'Image'),
            'video_title' => Yii::t('app', 'Video Title'),
            'link_rewrite' => Yii::t('app', 'Link Rewrite'),
            'add_date' => Yii::t('app', 'Add Date'),
            'update_date' => Yii::t('app', 'Update Date'),
            'created_by_id' => Yii::t('app', 'Created By ID'),
            'updated_by_id' => Yii::t('app', 'Updated By ID'),
            'view_count' => Yii::t('app', 'View Count'),
            'status' => Yii::t('app', 'Status'),
            'video_caption' => Yii::t('app', 'Video Caption'),
            'coverImage' => Yii::t('app', 'Cover Image'),
            'videoUpload' => Yii::t('app', 'Video Upload'),
        ];
    }

    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $this->link_rewrite = Common::getUrlGenerater($this->video_title);
        }
        return parent::beforeSave($insert);
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id_product' => 'id_product']);
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(Model::className(), ['id_model' => 'id_model']);
    }

}