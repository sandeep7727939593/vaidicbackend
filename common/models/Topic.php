<?php

namespace common\models;

use Yii;
use common\helpers\Common;
use common\models\Category;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "topic".
 *
 * @property int $id_topic
 * @property string $title
 * @property string $link_rewrite
 * @property string $side_menu_title
 * @property string $description
 * @property int $id_category
 * @property int|null $status
 * @property int|null $index_position
 * @property string|null $created_by_id
 * @property string|null $updated_by_id
 * @property string|null $add_date
 * @property string $update_date
 */
class Topic extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'topic';
    }

    /**
     * Behaviours for updated_date,created_date,add_date,logger behaviour
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['add_date','update_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'update_date',
                ],
                'value' => function ($event) {                    
                    return date('Y-m-d H:i:s');                    
                },                
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'side_menu_title', 'description', 'id_category'], 'required'],
            [['description'], 'string'],
            [['id_category', 'status', 'index_position'], 'integer'],
            [['add_date', 'update_date'], 'safe'],
            [['title', 'link_rewrite'], 'string', 'max' => 500],
            [['side_menu_title'], 'string', 'max' => 100],
            [['created_by_id', 'updated_by_id'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_topic' => 'Id Topic',
            'title' => 'Title',
            'link_rewrite' => 'Link Rewrite',
            'side_menu_title' => 'Side Menu Title',
            'description' => 'Description',
            'id_category' => 'Category Name',
            'status' => 'Status',
            'index_position' => 'Index Position',
            'created_by_id' => 'Created By ID',
            'updated_by_id' => 'Updated By ID',
            'add_date' => 'Add Date',
            'update_date' => 'Update Date',
        ];
    }


    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $this->link_rewrite = Common::getUrlGenerater($this->title);
        }
        return parent::beforeSave($insert);
    }

    public function getCategory()
    {
         return $this->hasOne(Category::className(), ['id_category' => 'id_category']);
    }

}