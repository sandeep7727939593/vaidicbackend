<?php

namespace common\models;

use Yii;
use common\helpers\Common;
use common\models\Model;
use common\models\Product;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "{{%news}}".
 *
 * @property int $id_news
 * @property int $id_product
 * @property int $id_model
 * @property string|null $content
 * @property string|null $id_author
 * @property string|null $image
 * @property string|null $title
 * @property string|null $link_rewrite
 * @property string $add_date
 * @property string $update_date
 * @property int|null $created_by_id
 * @property int|null $updated_by_id
 * @property int|null $view_count
 * @property int|null $status
 */
class News extends \yii\db\ActiveRecord
{
    public $coverImage;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%news}}';
    }

    /**
     * Behaviours for updated_date,created_date,add_date,logger behaviour
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['add_date','update_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'update_date',
                ],
                'value' => function ($event) {                    
                    return date('Y-m-d H:i:s');                    
                },                
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_product', 'id_model', 'id_author', 'title', 'content'], 'required'],
            [['id_product', 'id_model', 'created_by_id', 'updated_by_id', 'view_count', 'status'], 'integer'],
            [['content', 'id_author', 'image'], 'string'],
            [['add_date', 'update_date'], 'safe'],
            [['title', 'link_rewrite'], 'string', 'max' => 500],
            [['coverImage'], 'image', 'extensions' => ['jpg', 'jepg', 'png']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_news' => Yii::t('app', 'Id News'),
            'id_product' => Yii::t('app', 'Product'),
            'id_model' => Yii::t('app', 'Model'),
            'content' => Yii::t('app', 'Content'),
            'id_author' => Yii::t('app', 'Author'),
            'image' => Yii::t('app', 'Image'),
            'title' => Yii::t('app', 'Title'),
            'link_rewrite' => Yii::t('app', 'Link Rewrite'),
            'add_date' => Yii::t('app', 'Add Date'),
            'update_date' => Yii::t('app', 'Update Date'),
            'created_by_id' => Yii::t('app', 'Created By ID'),
            'updated_by_id' => Yii::t('app', 'Updated By ID'),
            'view_count' => Yii::t('app', 'View Count'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $this->link_rewrite = Common::getUrlGenerater($this->title);
        }
        return parent::beforeSave($insert);
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id_product' => 'id_product']);
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(Model::className(), ['id_model' => 'id_model']);
    }

}