<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%specs}}".
 *
 * @property int $id_specs
 * @property int|null $id_model
 * @property int|null $id_product
 * @property string|null $specs
 * @property int|null $status
 * @property string|null $add_date
 * @property string|null $launch_date
 * @property string $update_date
 * @property string|null $created_by_id
 * @property string|null $updated_by_id
 */
class Specs extends \yii\db\ActiveRecord
{
    public $name;
    public $value;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%specs}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_model', 'id_product', 'status'], 'integer'],
            [['specs', 'name', 'value'], 'string'],
            [['add_date', 'launch_date', 'update_date'], 'safe'],
            [['created_by_id', 'updated_by_id'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_specs' => Yii::t('app', 'Id Specs'),
            'id_model' => Yii::t('app', 'Id Model'),
            'id_product' => Yii::t('app', 'Id Product'),
            'specs' => Yii::t('app', 'Specs'),
            'status' => Yii::t('app', 'Status'),
            'add_date' => Yii::t('app', 'Add Date'),
            'launch_date' => Yii::t('app', 'Launch Date'),
            'update_date' => Yii::t('app', 'Update Date'),
            'created_by_id' => Yii::t('app', 'Created By ID'),
            'updated_by_id' => Yii::t('app', 'Updated By ID'),
        ];
    }

    /**
     * Creates and populates a set of models.
     *
     * @param string $modelClass
     * @param array $multipleModels
     * @return array
     */
    public static function createMultiple($modelClass, $multipleModels = [])
    {
        $model    = new $modelClass;
        $formName = $model->formName();
        $post     = Yii::$app->request->post($formName);
        $models   = [];

        if (! empty($multipleModels)) {
            $keys = array_keys(ArrayHelper::map($multipleModels, 'id', 'id'));
            $multipleModels = array_combine($keys, $multipleModels);
        }

        if ($post && is_array($post)) {
            foreach ($post as $i => $item) {
                if (isset($item['id']) && !empty($item['id']) && isset($multipleModels[$item['id']])) {
                    $models[] = $multipleModels[$item['id']];
                } else {
                    $models[] = new $modelClass;
                }
            }
        }

        unset($model, $formName, $post);

        return $models;
    }
}