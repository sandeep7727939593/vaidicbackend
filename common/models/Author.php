<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%author}}".
 *
 * @property int $id_author Primary key for the table
 * @property string $email_id Email id of the author
 * @property string|null $twitter
 * @property string|null $linked_in
 * @property string $description Description about the author
 * @property string|null $image_url Provides image url of the author
 * @property int $no_of_viewers
 * @property string|null $author_email_id
 * @property string $start_date Provide registration date for author
 * @property string|null $end_date Provide end date of author
 * @property string $update_date Provide update_date of the author
 * @property string $name
 * @property string $link_rewrite
 * @property string $city
 * @property string|null $mobileno
 * @property int|null $is_blogger
 * @property int|null $created_by_id
 * @property int|null $updated_by_id
 */
class Author extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%author}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email_id', 'description', 'start_date', 'update_date', 'name', 'link_rewrite', 'city'], 'required'],
            [['description'], 'string'],
            [['no_of_viewers', 'is_blogger', 'created_by_id', 'updated_by_id'], 'integer'],
            [['start_date', 'end_date', 'update_date'], 'safe'],
            [['email_id', 'twitter', 'linked_in', 'author_email_id'], 'string', 'max' => 45],
            [['image_url', 'name', 'link_rewrite', 'city'], 'string', 'max' => 255],
            [['mobileno'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_author' => Yii::t('app', 'Id Author'),
            'email_id' => Yii::t('app', 'Email ID'),
            'twitter' => Yii::t('app', 'Twitter'),
            'linked_in' => Yii::t('app', 'Linked In'),
            'description' => Yii::t('app', 'Description'),
            'image_url' => Yii::t('app', 'Image Url'),
            'no_of_viewers' => Yii::t('app', 'No Of Viewers'),
            'author_email_id' => Yii::t('app', 'Author Email ID'),
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'update_date' => Yii::t('app', 'Update Date'),
            'name' => Yii::t('app', 'Name'),
            'link_rewrite' => Yii::t('app', 'Link Rewrite'),
            'city' => Yii::t('app', 'City'),
            'mobileno' => Yii::t('app', 'Mobileno'),
            'is_blogger' => Yii::t('app', 'Is Blogger'),
            'created_by_id' => Yii::t('app', 'Created By ID'),
            'updated_by_id' => Yii::t('app', 'Updated By ID'),
        ];
    }
}