<?php

namespace common\models;

use Yii;
use common\helpers\Common;
use common\models\Product;
use common\models\Model;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "{{%benefit}}".
 *
 * @property int $id_benefit
 * @property int|null $id_product
 * @property int|null $id_model
 * @property string|null $link_rewrite
 * @property string|null $question
 * @property string|null $answer
 * @property int|null $status
 * @property string|null $add_date
 * @property string|null $launch_date
 * @property string $update_date
 * @property string|null $created_by_id
 * @property string|null $updated_by_id
 */
class Benefit extends \yii\db\ActiveRecord
{
    public $title;
    public $description;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%benefit}}';
    }

    /**
     * Behaviours for updated_date,created_date,add_date,logger behaviour
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['add_date','update_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'update_date',
                ],
                'value' => function ($event) {                    
                    return date('Y-m-d H:i:s');                    
                },                
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_product', 'id_model', 'status'], 'safe'],
            [['value'], 'string'],
            [['add_date', 'launch_date', 'update_date', 'title', 'description'], 'safe'],
            [['created_by_id', 'updated_by_id'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_benefit' => Yii::t('app', 'Id Benefit'),
            'id_product' => Yii::t('app', 'Id Product'),
            'id_model' => Yii::t('app', 'Id Model'),
            'value' => Yii::t('app', 'Value'),
            'status' => Yii::t('app', 'Status'),
            'add_date' => Yii::t('app', 'Add Date'),
            'launch_date' => Yii::t('app', 'Launch Date'),
            'update_date' => Yii::t('app', 'Update Date'),
            'created_by_id' => Yii::t('app', 'Created By ID'),
            'updated_by_id' => Yii::t('app', 'Updated By ID'),
        ];
    }

    // public function beforeSave($insert) {
    //     if ($this->isNewRecord) {
    //         $this->link_rewrite = Common::getUrlGenerater($this->question);
    //     }
    //     return parent::beforeSave($insert);
    // }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id_product' => 'id_product']);
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getModel()
    {
        return $this->hasOne(Model::className(), ['id_model' => 'id_model']);
    }


    /**
     * Creates and populates a set of models.
     *
     * @param string $modelClass
     * @param array $multipleModels
     * @return array
     */
    public static function createMultiple($modelClass, $multipleModels = [])
    {
        $model    = new $modelClass;
        $formName = $model->formName();
        $post     = Yii::$app->request->post($formName);
        $models   = [];

        if (! empty($multipleModels)) {
            $keys = array_keys(ArrayHelper::map($multipleModels, 'id', 'id'));
            $multipleModels = array_combine($keys, $multipleModels);
        }

        if ($post && is_array($post)) {
            foreach ($post as $i => $item) {
                if (isset($item['id']) && !empty($item['id']) && isset($multipleModels[$item['id']])) {
                    $models[] = $multipleModels[$item['id']];
                } else {
                    $models[] = new $modelClass;
                }
            }
        }

        unset($model, $formName, $post);

        return $models;
    }
}


