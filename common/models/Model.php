<?php

namespace common\models;

use Yii;
use common\helpers\Common;
use common\models\Product;
use common\models\Specs;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
/**
 * This is the model class for table "{{%model}}".
 *
 * @property int $id_model
 * @property int|null $id_product
 * @property string|null $cover_image
 * @property string|null $model_name
 * @property string|null $short_name
 * @property string|null $link_rewrite
 * @property int|null $base_price
 * @property string|null $description
 * @property int|null $popularity
 * @property string|null $alternative_link_rewrite
 * @property string|null $alternative_link_type
 * @property int|null $status
 * @property string|null $add_date
 * @property string|null $launch_date
 * @property string $update_date
 * @property string|null $created_by_id
 * @property string|null $updated_by_id
 */
class Model extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%model}}';
    }
     /**
     * Behaviours for updated_date,created_date,add_date,logger behaviour
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['add_date','update_date'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'update_date',
                ],
                'value' => function ($event) {                    
                    return date('Y-m-d H:i:s');                    
                },                
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_product', 'base_price', 'launch_date', 'description', 'model_name'], 'required'],
            [['id_product', 'base_price', 'popularity', 'status'], 'integer'],
            [['description'], 'string'],
            [['add_date', 'launch_date', 'update_date'], 'safe'],
            [['file'], 'string', 'max' => 500],
            [['model_name', 'short_name', 'link_rewrite', 'created_by_id', 'updated_by_id'], 'string', 'max' => 50],
            [['alternative_link_rewrite'], 'string', 'max' => 100],
            [['alternative_link_type'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_model' => Yii::t('app', 'Id Model'),
            'id_product' => Yii::t('app', 'Product'),
            'cover_image' => Yii::t('app', 'Cover Image'),
            'file' => Yii::t('app', 'Cover Image'),
            'model_name' => Yii::t('app', 'Model Name'),
            'short_name' => Yii::t('app', 'Short Name'),
            'link_rewrite' => Yii::t('app', 'Link Rewrite'),
            'base_price' => Yii::t('app', 'Base Price'),
            'description' => Yii::t('app', 'Description'),
            'popularity' => Yii::t('app', 'Popularity'),
            'alternative_link_rewrite' => Yii::t('app', 'Alternative Link Rewrite'),
            'alternative_link_type' => Yii::t('app', 'Alternative Link Type'),
            'status' => Yii::t('app', 'Status'),
            'add_date' => Yii::t('app', 'Add Date'),
            'launch_date' => Yii::t('app', 'Launch Date'),
            'update_date' => Yii::t('app', 'Update Date'),
            'created_by_id' => Yii::t('app', 'Created By ID'),
            'updated_by_id' => Yii::t('app', 'Updated By ID'),
        ];
    }

    public function beforeSave($insert) {
        if ($this->isNewRecord) {
            $this->link_rewrite = Common::getUrlGenerater($this->model_name);
        }
        return parent::beforeSave($insert);
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id_product' => 'id_product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecs()
    {
        return $this->hasOne(Specs::className(), ['id_model' => 'id_model']);
    }


    public static function dropdown()
    {
        $dropdown = ['' => 'Search Model'];
        $product = Model::find()->asArray()->all();
        foreach ($product as $p) {
            $dropdown[$p['id_model']] = $p['model_name'];
        }
        return $dropdown;
    }
}