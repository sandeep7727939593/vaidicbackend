<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\helpers\Common;

/**
 * Site controller
 */
class TopicController extends Controller {

	/**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex($category)
    {
        $topicService = Yii::$container->get('frontend\services\TopicServiceInterface');
        $categoryService = Yii::$container->get('frontend\services\CategoryServiceInterface');
        $topic = Yii::$app->request->get('topic', '');
        $categoryData = $categoryService->getQuery(['link_rewrite' => $category], [], 1, '', 1, null, [], ['as_array' => true]);
        $filter = [
            'topic.id_category' => $categoryData['id_category'],
            'topic.status' => 1
        ];
        if(!empty($topic)) {
            $filter['topic.link_rewrite'] = $topic;
        }
        $field = [
            'title',
            'topic.link_rewrite',
            'side_menu_title',
            'description',
            'topic.id_category',
            'category.link_rewrite as category_link_rewrite',
            'category',
        ];
        $join = [
            ['INNER JOIN', 'category', 'category.id_category=category.id_category']
        ];
        $topicData = $topicService->getQuery($filter, $field, 1, '', 1, null, $join, ['as_array' => true]);
        if(empty($topicData)) {
            return $this->redirect(['site/index']);
        }
        $topicSide = $topicService->getQuery(['id_category' => $topicData['id_category']], [], 1, '', -1, null, [], ['as_array' => true]);
        $siteNav = [];
        foreach ($topicSide as $key => $value) {
            $siteNav[] = [
                'url' => Common::getCategoryUrl($topicData['category_link_rewrite'], $value['link_rewrite'], false), 
                'label' => $value['side_menu_title'], 
                'icon' => 'arrow-right'
            ];
        }
        $data = [
            'categoryName' => $topicData['category'],
            'title' => $topicData['title'],
            'content' => $topicData['description'],
            'siteNav' => $siteNav,
            
        ];
        // echo "<pre>"; print_r($data);die;
        return $this->render('index', $data);
    }

}?>