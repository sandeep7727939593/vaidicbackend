<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\helpers\Common;

/**
 * Site controller
 */
class AjaxController extends Controller {

	/**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionHeaderCategory()
    {
    	$categoryService = Yii::$container->get('frontend\services\CategoryServiceInterface');
        $categoryData = $categoryService->getQuery([], ['category', 'link_rewrite'], 1, '', 9, null, [], ['as_array' => true]);
        $html = '';
        $time = .3;
        foreach ($categoryData as $key => $value) {
            $url = Common::getCategoryUrl($value['link_rewrite']);
        	$html .='<a href="' . $url . '" data-wow-duration="1s" data-wow-delay="' . $time . 's" class="primary-btn transparent mr-10 mb-10 wow fadeInDown">' . $value['category'] . '</a>';
        	$time = $time + .3;
        }
        return $html;
    }

}?>