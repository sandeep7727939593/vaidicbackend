<?php
$container = Yii::$container;
$container->set('frontend\services\BaseServiceInterface', [
    'class' => 'frontend\services\BaseServices',
]);
$container->set('frontend\services\CategoryServiceInterface', [
    'class' => 'frontend\services\CategoryServices',
]);
$container->set('frontend\services\TopicServiceInterface', [
    'class' => 'frontend\services\TopicServices',
]);
