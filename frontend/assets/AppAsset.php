<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        "css/linearicons.css",
        "css/font-awesome.min.css",
        "css/bootstrap.css",
        "css/magnific-popup.css",
        "css/nice-select.css",
        "css/animate.min.css",
        "css/owl.carousel.css",
        "css/main.css",
    ];
    public $js = [
        'js/main.js',
        "js/easing.min.js",
        "js/hoverIntent.js",
        "js/superfish.min.js",
        "js/jquery.ajaxchimp.min.js",
        "js/jquery.magnific-popup.min.js",
        "js/owl.carousel.min.js",
        "js/owl-carousel-thumb.min.js",
        "js/jquery.sticky.js",
        "js/jquery.nice-select.min.js",
        "js/parallax.min.js",
        "js/waypoints.min.js",
        "js/wow.min.js",
        "js/jquery.counterup.min.js",
        "js/mail-script.js",
        "js/main.js",
    ];
    public $depends = [
        // 'yii\web\YiiAsset',
        // 'yii\bootstrap\BootstrapAsset',
    ];
}
