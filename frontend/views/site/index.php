
    <!-- Start About Area -->
    <section class="about-area section-gap">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-lg-5 col-md-6 about-left">
                    <img class="img-fluid" src="img/about.jpg" alt="">
                </div>
                <div class="offset-lg-1 col-lg-6 offset-md-0 col-md-12 about-right">
                    <h1>
                        Over 2500 Courses <br> from 5 Platform
                    </h1>
                    <div class="wow fadeIn" data-wow-duration="1s">
                        <p>
                            There is a moment in the life of any aspiring astronomer that it is time to buy that first telescope. It’s exciting to think
                            about setting up your own viewing station. In the life of any aspiring astronomer that it is time to buy that first
                            telescope. It’s exciting to think about setting up your own viewing station.
                        </p>
                    </div>
                    <a href="courses.html" class="primary-btn">Explore Courses</a>
                </div>
            </div>
        </div>
    </section>
    <!-- End About Area -->


    <!-- Start Courses Area -->
    <section class="courses-area section-gap">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 about-right">
                    <h1>
                        This is Why <br> We have Solid Idea
                    </h1>
                    <div class="wow fadeIn" data-wow-duration="1s">
                        <p>
                            There is a moment in the life of any aspiring astronomer that it is time to buy that first telescope. It’s exciting to think
                            about setting up your own viewing station. In the life of any aspiring astronomer that it is time to buy that first
                            telescope. It’s exciting to think about setting up your own viewing station.
                        </p>
                    </div>
                    <a href="courses.html" class="primary-btn white">Explore Courses</a>
                </div>
                <div class="offset-lg-1 col-lg-6">
                    <div class="courses-right">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <ul class="courses-list">
                                    <?php 
                                    $count = count($categoryData)/2;
                                    $counter = .1;
                                    foreach ($categoryData as $key => $value) { 
                                        if ($key >= $count) {
                                            continue;
                                        }
                                        ?>
                                        <li>
                                            <a href=<?= $value['url']; ?> class="wow fadeInLeft"  data-wow-duration="1s" data-wow-delay=<?= "{$counter}s"?>>
                                                <i class="fa fa-book"></i> <?= $value['category']?>
                                            </a>
                                        </li>
                                    <?php $counter = $counter + .2; }?>
                                </ul>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <ul class="courses-list">
                                    <?php 
                                    $count = count($categoryData)/2;
                                    foreach ($categoryData as $key => $value) { 
                                        if ($key < $count) {
                                            continue;
                                        }
                                        $counter = $counter - .2; ?>
                                        <li>
                                            <a href=<?= $value['url'];?> class="wow fadeInLeft"  data-wow-duration="1s" data-wow-delay=<?= "{$counter}s"?>>
                                                <i class="fa fa-book"></i> <?= $value['category']?>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Courses Area -->


    <!--Start Feature Area -->
    <section class="feature-area">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="section-title text-center">
                        <h1>Features That Make Us Hero</h1>
                        <p>
                            If you are looking at blank cassettes on the web, you may be very confused at the difference in price. You may see some for
                            as low as $.17 each.
                        </p>
                    </div>
                </div>
            </div>
            <div class="feature-inner row">
                <div class="col-lg-4 col-md-6">
                    <div class="feature-item">
                        <i class="ti-crown"></i>
                        <h4>Developer</h4>
                        <div class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".1s">
                            <p>
                                Lorem ipsum dolor sit amet consec tetur adipisicing elit, sed do eiusmod tempor incididunt labore.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="feature-item">
                        <i class="ti-briefcase"></i>
                        <h4>Testing</h4>
                        <div class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".3s">
                            <p>
                                Lorem ipsum dolor sit amet consec tetur adipisicing elit, sed do eiusmod tempor incididunt labore.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="feature-item">
                        <i class="ti-medall-alt"></i>
                        <h4>MIS</h4>
                        <div class="wow fadeIn" data-wow-duration="1s" data-wow-delay=".5s">
                            <p>
                                Lorem ipsum dolor sit amet consec tetur adipisicing elit, sed do eiusmod tempor incididunt labore.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- End Feature Area -->



    <!-- ####################### Start Scroll to Top Area ####################### -->
    <div id="back-top">
        <a title="Go to Top" href="#"></a>
    </div>
    <!-- ####################### End Scroll to Top Area ####################### -->