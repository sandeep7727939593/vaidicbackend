<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
        <!--
            Google Font
            ============================================= -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,500,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500i" rel="stylesheet">

    <!--
            CSS
            ============================================= -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/themify-icons/0.1.2/css/themify-icons.css">

</head>
<body>
<?php $this->beginBody() ?>
    <header id="header">
        <div class="container">
            <div class="row align-items-center justify-content-between d-flex">
                <div id="logo">
                    <a href="/"><img src="images/logo.png" alt="" title="" /></a>
                </div>
                <nav id="nav-menu-container">
                    <ul class="nav-menu">
                        <li class="menu-active"><a href="/">Home</a></li>
                        <li><a href="about">About</a></li>
                        <li class="menu-has-children"><a href="/all-topic">Topic</a>
                            <ul>
                                <li><a href="/recent-topic">Recent Topics</a></li>
                                <li><a href="/trending-topic">Trending Topics</a></li>
                                <li><a href="/difference-topic">Difference Topics</a></li>
                            </ul>
                        </li>
                        <li class="menu-has-children"><a href="/quiz">Quiz</a>
                        </li>
                        <li><a href="contact">Contact</a></li>
                    </ul>
                </nav><!-- #nav-menu-container -->
            </div>
        </div>
    </header>
    <!-- Start Banner Area -->
    <section class="home-banner-area relative">
        <div class="container">
            <div class="row fullscreen d-flex align-items-center justify-content-center">
                <div class="banner-content col-lg-8 col-md-12">
                    <h1 class="wow fadeIn" data-wow-duration="4s">There's a way to do it better </br>- find it.</h1>
                    <p class="text-white">
                        “Live as if you were to die tomorrow. Learn as if you were to live forever.” ― Mahatma Gandhi
                    </p>

                    <div class="input-wrap">
                        <form action="" class="form-box d-flex justify-content-between">
                            <input type="text" placeholder="Search Topics" class="form-control" name="serach">
                            <button type="submit" class="btn search-btn">Search</button>
                        </form>
                    </div>
                    <h4 class="text-white">Top Subject Topics</h4>

                    <div class="courses pt-20" id="header-category">
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="rocket-img">
            <img src="images/rocket.png" alt="">
        </div>
    </section>
    <!-- End Banner Area -->
<div class="wrap">

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>


    <!-- Start Footer Area -->
    <footer class="footer-area section-gap">
        <div class="container">
            
            <div class="footer-bottom row align-items-center">
                <p class="footer-text m-0 col-lg-8 col-md-12">
                <div class="col-lg-4 col-md-12 footer-social">
                    Sandeep Kumar Jangir
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer Area -->
    <script src="js/vendor/jquery-2.2.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
     crossorigin="anonymous"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
