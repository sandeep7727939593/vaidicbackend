<?php

namespace frontend\services;

use frontend\services\BaseServiceInterface;
use common\models\dao\BaseDaoInterface;

class BaseServices {

    public $_dao;

    public function getQuery($filters = [], $fields = [], $page = 1, $order = '', $limit = 1000, $group= null, $join = [], $additionalParams = [])
    {
    	 try {
            $params = [
                'page' => $page,
                'order' => $order,
                'limit' => $limit,
                'group' => $group,
                'join' => $join,
                'having' => !empty($additionalParams['having']) ? $additionalParams['having'] : [],
            ];
            $params = array_merge($params, $additionalParams);
            $asArray = isset($additionalParams['as_array']) && $additionalParams['as_array'] ? true : false;
            $forceSelectAll = isset($additionalParams['forceSelectAll']) && $additionalParams['forceSelectAll'] ? true : false;
            if ($asArray) {
                return $limit == 1 && !$forceSelectAll ? $this->_dao->selectAsArray($fields, $filters, $params) : $this->_dao->selectAllDataAsArray($fields, $filters, $params);
            } else {
                return $limit == 1 && !$forceSelectAll ? $this->_dao->select($fields, $filters, $params) : $this->_dao->selectAll($fields, $filters, $params);
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), 400, $e);
        }
    }
}

?>