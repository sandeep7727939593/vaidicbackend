<?php

namespace frontend\services;

use frontend\services\BaseServices;
use frontend\services\TopicServiceInterface;
use common\models\dao\TopicDaoInterface;

class TopicServices extends BaseServices implements TopicServiceInterface {


    /**
     * Default constructor
     *
     * @param TopicDaoInterface $topicDao topicDao
     */
    public function __construct(TopicDaoInterface $topicDao)
    {
        $this->_dao = $topicDao;
    }
}

?>