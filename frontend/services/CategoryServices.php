<?php

namespace frontend\services;

use frontend\services\BaseServices;
use frontend\services\CategoryServiceInterface;
use common\models\dao\CategoryDaoInterface;

class CategoryServices extends BaseServices implements CategoryServiceInterface {


    /**
     * Default constructor
     *
     * @param CategoryDaoInterface $categoryDao categoryDao
     */
    public function __construct(CategoryDaoInterface $categoryDao)
    {
        $this->_dao = $categoryDao;
    }
}

?>